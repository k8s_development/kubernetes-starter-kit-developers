

https://www.digitalocean.com/community/developer-center/how-to-install-loki-stack-in-doks-cluster

k create ns loki-stack

export KUBECONFIG=/home/juno/repos/gitlab.com/kadry/devops/k8s/k3s.yaml && kubectl config set-context --current --namespace loki

HELM_CHART_VERSION="2.6.4" helm install loki grafana/loki-stack --version "${HELM_CHART_VERSION}" \
  --namespace=loki-stack \
  --create-namespace \
  -f "04-setup-observability/assets/manifests/loki-stack-values-v${HELM_CHART_VERSION}.yaml"

helm install loki grafana/loki-stack --version "2.6.4" \
  --namespace=loki \
  --create-namespace \
  -f "04-setup-observability/assets/manifests/loki-stack-values-v2.6.4.yaml" --set grafana.rbac.pspEnabled=false --set loki.rbac.pspEnabled=false 

helm install loki grafana/loki-stack --version "2.6.4" \
  --namespace=loki \
  -f "04-setup-observability/assets/manifests/loki-stack-values-v2.6.4.yaml" --set grafana.rbac.pspEnabled=false --set loki.rbac.pspEnabled=false 

helm upgrade loki grafana/loki-stack --version "2.6.4" \
  --namespace=loki \
  -f "04-setup-observability/assets/manifests/loki-stack-values-v2.6.4.yaml" --set grafana.rbac.pspEnabled=false --set loki.rbac.pspEnabled=false



helm upgrade --install --namespace=loki grafana grafana/grafana


grafana.loki.svc.cluster.local
http://loki.loki.svc.cluster.local:3100

3kUZBuMLSHnR5zjAL95TW2P20Y5H9p8vcftCZBhz